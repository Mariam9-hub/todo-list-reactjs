import React from "react";

const Cellule = ({ number}) => {
  var color = "";
  var r = 257;
  var g = 257;
  var rouge= 0 ;
  var vert = 0;
  if (number > 0) {
    for (var i = 100; i > 0; i--) {
      r -= 1;
      g = 0;
      if (number === i) {
          rouge = Math.abs(i)+150;
          vert = 0;
          console.log(i,Math.abs(i))
       /* r = Math.abs(r).toString(16);
        g = Math.abs(g).toString(16);
        if (r.length === 1) r = "0" + r;
        if (g.length === 1) g = "0" + g;
        color = "#" + r + g + "00";
        console.log(color);*/
      }
    }
  } else {
    for (var j = -100; j < 1; j++) {
      g -= 2;
      r = 0;
      if (number === j) {
        vert = Math.abs(j)+150;
        rouge = 0;

        /*r = Math.abs(r).toString(16);
        g = Math.abs(g).toString(16);
        if (r.length === 1) r = "0" + r;
        if (g.length === 1) g = "0" + g;
        color = "#" + r + g + "00";
        console.log(color);*/
      }
    }
  }

  return (
    <div>
        <h1>{`rgba(${rouge},${vert},0)`}</h1>
      <svg width="70px" height="80px" xmlns="http://www.w3.org/2000/svg">
        <g>
          <rect x="0px" y="0px" width="50" height="50" style={ {fill:`rgba(${rouge},${vert},0)`}}>
          </rect>
          <text
            x="12.5px"
            y="25px"
            fontFamily="Verdana"
            fontSize="15"
            fill="#F4F4F4"
          >
            {number}
          </text>
        </g>
      </svg>
    </div>
  );
};
export default Cellule;
